from search.models import Entry, Facet, Results
from search.search import search

entry1 = Entry(
    title="Legend: The Best of Bob Marley and the Wailers (Remastered)",
    release_date="2002-01-01T00:00:00-07:00",
    price=9.99,
)
entry2 = Entry(
    title="The Very Best of The Doors",
    release_date="2008-01-29T00:00:00-07:00",
    price=19.99,
)
entries = [entry1, entry2]


def test_search_by_keyword():
    assert search(entries, "best", {}).items == entries


def test_search_by_title():
    assert search(
        entries, "Legend: The Best of Bob Marley and the Wailers", {}
    ).items == [entry1]


def test_price_facet_generation():
    assert [v.value for v in search(entries, "best", {}).facets["price"]] == [
        "5 - 10",
        "15 - 20",
    ]


def test_year_facet_generation():
    assert [v.value for v in search(entries, "best", {}).facets["year"]] == [
        "2008",
        "2002",
    ]


def test_filter_multiple_facet_values():
    result = search(entries, "best", {"year": ["2002", "2008"]})

    assert result.items == entries
    assert [v.value for v in result.facets["year"]] == ["2008", "2002"]
    assert [v.value for v in result.facets["price"]] == ["5 - 10", "15 - 20"]


def test_filter_multiple_facets():
    result = search(entries, "best", {"year": ["2002"], "price": ["5 - 10"]})

    assert result.items == [entry1]
    assert [v.value for v in result.facets["year"]] == ["2002"]
    assert [v.value for v in result.facets["price"]] == ["5 - 10"]


def test_year_facet_count():
    assert search(entries, "best", {"year": ["2002"]}) == Results(
        items=[entry1],
        facets={
            "year": [
                Facet("2008", 1),
                Facet("2002", 1),
            ],
            "price": [Facet("5 - 10", 1)],
        },
        query="best",
    )


def test_price_facet_count():
    assert search(entries, "best", {"price": ["5 - 10",]}) == Results(
        items=[entry1],
        facets={
            "year": [
                Facet("2002", 1),
            ],
            "price": [Facet("5 - 10", 1), Facet("15 - 20", 1)],
        },
        query="best",
    )


def test_facet_count_multiple_values():
    assert search(entries, "best", {"year": ["2002", "2008"]}) == Results(
        items=entries,
        facets={
            "year": [
                Facet("2008", 1),
                Facet("2002", 1),
            ],
            "price": [
                Facet("5 - 10", 1),
                Facet("15 - 20", 1),
            ],
        },
        query="best",
    )
