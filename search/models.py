from dataclasses import dataclass
from typing import Optional, List, Dict


@dataclass
class Entry:
    title: str
    price: float
    release_date: str
    link: Optional[str] = None
    image: Optional[str] = None

    @classmethod
    def from_response(cls, data: dict) -> List["Entry"]:
        return [
            Entry(
                title=v["title"]["label"],
                link=v["link"]["attributes"]["href"],
                price=float(v["im:price"]["attributes"]["amount"]),
                release_date=v["im:releaseDate"]["label"],
                image=next(iter([i["label"] for i in v.get("im:image", [])]), None),
            )
            for v in data["feed"]["entry"]
        ]


@dataclass
class Facet:
    value: str
    count: int


@dataclass
class Results:
    items: List[Entry]
    facets: Dict[str, List[Facet]]
    query: str
