import re
from urllib import request
import json
from typing import List, Dict, Set

from .models import Entry, Results, Facet


def load_entries() -> List[Entry]:
    with request.urlopen(
        "https://itunes.apple.com/us/rss/topalbums/limit=200/json"
    ) as f:
        return Entry.from_response(json.load(f))


def search(entries: List[Entry], query: str, filters: Dict[str, List[str]]) -> Results:
    print(f"Searching for {query} with filters {filters}")
    return Results(
        # TODO: filter by selected facets
        items=filter_by_query(entries, query) if query else entries,
        # TODO: generate facets from matched documents data
        facets={
            "year": [
                Facet("2015", 25),
                Facet("2017", 2),
                Facet("2018", 54),
            ],
            "price": [
                Facet("5 - 10", 25),
                Facet("10 - 15", 2),
                Facet("20 - 25", 54),
            ],
        },
        query=query,
    )


def tokenize(value: str) -> Set[str]:
    return {v.lower() for v in re.findall(r"[\w']+", value)}


def filter_by_query(entries: List[Entry], query: str) -> List[Entry]:
    query_tokens = tokenize(query)
    return [v for v in entries if query_tokens & tokenize(v.title) == query_tokens]
