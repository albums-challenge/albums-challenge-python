from fastapi import FastAPI, Request, Query
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
from fastapi.responses import HTMLResponse
from typing import Optional, Dict, List

from .search import search, load_entries
from .models import Results

app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")

templates = Jinja2Templates(directory="templates")
entries = load_entries()


@app.get("/", response_class=HTMLResponse)
async def index_page(request: Request):
    return templates.TemplateResponse("index.html", {"request": request})


@app.get("/api/search", response_model=Results)
async def search_api(
    query: str = Query(...),
    year: Optional[List[str]] = Query(None),
    price: Optional[List[str]] = Query(None),
) -> Results:
    return search(entries, query, {"year": year or [], "price": price or []})
